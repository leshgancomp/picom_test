<?php
error_reporting(E_ERROR);

define ('CACHE_PATH',__DIR__.'/cache');
define ('DB_FILE',__DIR__.'/users');

spl_autoload_register(function ($class) {
    if (is_file(__DIR__.'/'. $class . '.php'))
        require __DIR__.'/'. $class . '.php';
    else
        throw new Exception('Невозможно загрузить '.$class);    
});

function dump($var) {
    echo '<pre>' . print_r($var, true) . '</pre>';
}


//Подготовка закончена )))

use Controller\DbController;
use Controller\FileController;

foreach ($_GET as $k=>$v){
    $_GET[$k]=filter_var(trim($v),FILTER_SANITIZE_URL); //Почистим GET
}

/*
 * По уму здесь должен быть вызов роутера а из него вызов методов контроллера. Но думаю знания ясны и без этого ))) будем просто исходить из GET параметра и напрямую вызывать метод в контроллере.
 */

$action = $_GET['action']?$_GET['action']:'generate_db'; //Не будем использовать далле в секции default так как generate_db присваиваем только в случае пустого параметра

switch ($action) {
    case 'generate_db':
        $controller = new DbController;
        $controller->generateDbAction();
        break;
    case 'remove_from_db':
        $uuid = $_GET['uuid'];
        $controller = new DbController;
        $controller->removeFromDbAction($uuid);
        break;
    case 'load_from_db':
        $controller = new DbController;
        $controller->loadFromDbAction();
        break;
    case 'generate_file':
        $controller = new FileController;
        $controller->generateFileAction();
        break;
    case 'remove_from_file':
        $uuid = $_GET['uuid'];
        $controller = new FileController;
        $controller->removeFromFileAction($uuid);
        break;
    case 'load_from_file':
        $controller = new FileController;
        $controller->loadFromFileAction();
        break;
    default:
        break;
}

?>
