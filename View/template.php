<?php $action = $_GET['action']; ?>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <ul class="nav nav-pills">
                <li class="nav-item">
                    <a class="nav-link <?= ($action == 'load_from_file' ? 'active' : '')?>" href="/?action=load_from_file">Файл</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= ($action == 'load_from_db' ? 'active' : '')?>" href="/?action=load_from_db">База данных</a>
                </li>
            </ul>
        </nav>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Имя</th>
                    <th scope="col">Фамилия</th>
                    <th scope="col">Телефон</th>
                    <th scope="col">Email</th>
                    <th scope="col">Адрес</th>
                    <th scope="col">Зарегистрирован</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $number = 1;
                foreach ($users as $user):
                    $monthes = [
                        '01' => 'января',
                        '02' => 'февраля',
                        '03' => 'марта',
                        '04' => 'апреля',
                        '05' => 'мая',
                        '06' => 'июня',
                        '07' => 'июля',
                        '08' => 'августа',
                        '09' => 'сентября',
                        '10' => 'октября',
                        '11' => 'ноября',
                        '12' => 'декабря',
                    ];
                    $month = $monthes[$user->getRegisteredAt()->format('m')];?>
                    <tr>
                        <th scope="row"><?= $number++ ?></th>
                        <td><?= $user->getFirstName()?></td>
                        <td><?= $user->getLastName()?></td>
                        <td><?= $user->getPhone()?></td>
                        <td><?= $user->getEmail()?></td>
                        <td><?= implode(', ', $user->getLocation()) ?></td>
                        <td><?= $user->getRegisteredAt()->format('d ' . $month . ' Y H:i:s') ?></td>
                        <td>
                            <?php if ($action == 'load_from_db'):?>
                                <a href="/?action=remove_from_db&uuid=<?= $user->getUuid()?>" class="btn btn-danger">Удалить</a>
                            <?php else: ?>
                                <a href="/?action=remove_from_file&uuid=<?= $user->getUuid()?>" class="btn btn-danger">Удалить</a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="container-fluid">
            <?php if ($action == 'load_from_db') {?>
                <a class="btn btn-success float-right" href="/?action=generate_db" role="button">Сгенерировать в базе</a>
            <?php } else {?>
                <a class="btn btn-success float-right" href="/?action=generate_file" role="button">Сгенерировать в файле</a>
            <?php } ?>
        </div>


    </body>
</html>

