<?php

namespace Model;

class User {

    private $uuid;
    private $first_name;
    private $last_name;
    private $location;
    private $email;
    private $phone;
    private $registered_at;
    
    /*
     * Перенес генерацию uuid и registered_at в конструктор целенаправленно для последующей возможности изменения. Не смотря на то, что в коде используется заполнение этих значений
     */
    public function __construct() {
        $this->setUuid(uniqid())
             ->setRegisterdAt(new \DateTime("now"));
    }

    public function getUuid() {
        return $this->uuid;
    }

    public function setUuid($uuid) {
        $this->uuid = $uuid;
        return $this;
    }

    public function getFirstName() {
        return $this->first_name;
    }

    public function setFirstName($first_name) {
        $this->first_name = $first_name;
        return $this;
    }

    public function getLastName() {
        return $this->last_name;
    }

    public function setLastName($last_name) {
        $this->last_name = $last_name;
        return $this;
    }

    public function getLocation() {
        return $this->location;
    }

    public function setLocation($location) {
        $this->location = $location;
        return $this;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
        return $this;
    }

    public function getRegisteredAt() {
        return $this->registered_at;
    }

    public function setRegisterdAt($registeredAt) {
        $this->registered_at = $registeredAt;
        return $this;
    }
    
    public static function __set_state($an_array) {
        $obj = new User;
        $obj->uuid = $an_array['uuid'];
        $obj->first_name = $an_array['first_name'];
        $obj->last_name = $an_array['last_name'];
        $obj->location = $an_array['location'];
        $obj->email = $an_array['email'];
        $obj->phone = $an_array['phone'];
        $obj->registered_at = $an_array['registered_at'];
        return $obj;
    }

}
