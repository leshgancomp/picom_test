<?php

namespace Controller;

use Helper\Db\File as DbFile;
use Model\User;
use Helper\Cache;
use Helper\Logger;

class FileController {

    private $logger;

    public function __construct() {
        $this->logger = new Logger;
    }

    public function generateFileAction() {
        $database = new DbFile;
        $results = json_decode(file_get_contents('https://randomuser.me/api/?results=5&nat=gb'), true)['results'];
        if (isset($results) && is_array($results)) {
            $database->cleanDb();
            foreach ($results as $data) {
                $UserEntity = new User();
                $uuid = uniqid();
                $UserEntity->setUuid($uuid)
                        ->setFirstName($data['name']['first'])
                        ->setLastName($data['name']['last'])
                        ->setEmail($data['email'])
                        ->setPhone($data['phone'])
                        ->setLocation($data['location'])
                        ->setRegisterdAt(new \DateTime($data['registered']));
                $database->createUser($UserEntity);
            }
        }
        $this->logger->log('Fill file');
        header("Location: /?action=load_from_file");
    }
    
    public function removeFromFileAction($uuid) {
        $database = new DbFile;
        $User = $database->findUser($uuid);
        if ($User){
            $database->removeUser($User);
            $this->logger->log('Remove from file ' . $uuid);
        }
        header("Location: /?action=load_from_file");
    }
    
    public function loadFromFileAction() {
        $database = new DbFile;
        $users = $database->findAllUser();
        $this->logger->log('Load from file');
        include('View/template.php');
    }

}
