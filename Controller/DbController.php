<?php

namespace Controller;

use Helper\Db\Sqlite as DbSqlite;
use Model\User;
use Helper\Cache;
use Helper\Logger;

class DbController {

    private $logger;

    public function __construct() {
        $this->logger = new Logger;
    }

    public function generateDbAction() {
        $database = new DbSqlite;
        $results = json_decode(file_get_contents('https://randomuser.me/api/?results=5&nat=gb'), true)['results'];
        if (isset($results) && is_array($results)) {
            $database->cleanDb();
            foreach ($results as $data) {
                $UserEntity = new User();
                $uuid = uniqid();
                $UserEntity->setUuid($uuid)
                        ->setFirstName($data['name']['first'])
                        ->setLastName($data['name']['last'])
                        ->setEmail($data['email'])
                        ->setPhone($data['phone'])
                        ->setLocation($data['location'])
                        ->setRegisterdAt(new \DateTime($data['registered']));
                $database->createUser($UserEntity);
            }
        }
        Cache::reset('db');
        $this->logger->log('Fill db');
        header("Location: /?action=load_from_db");
    }
    
    public function removeFromDbAction($uuid) {
        $database = new DbSqlite;
        $User = $database->findUser($uuid);
        $database->removeUser($User);
        Cache::reset('db');
        $this->logger->log('Remove from db ' . $uuid);
        header("Location: /?action=load_from_db");
    }
    
    public function loadFromDbAction() {
        $users = Cache::get('db');
        if ($users === null) {
            $database = new DbSqlite;
            $users = $database->findAllUser();
            Cache::set('db', $users);
            $this->logger->log('Load from db');
        } else {
            $this->logger->log('Load from cache');
        }
        include('View/template.php');
    }

}
