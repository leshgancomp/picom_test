<?php

namespace Helper\Db;
use Model\User;

interface DbInterface {
    public function createUser(User $User);
    
    public function removeUser(User $User);
    
    public function findUser($uuid);
    
    public function findAllUser();
    
    public function cleanDb();
}

