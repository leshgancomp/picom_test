<?php

namespace Helper\Db;
use Helper\Db\DbInterface;
use Helper\Logger;
use Model\User;


class Sqlite implements DbInterface{
    
    private $pdo;
    private $logger;
    
    public function __construct() {
        $this->logger = new Logger();
        try {
            $this->pdo = new \PDO('sqlite:db.sqlite3');
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->pdo->exec('
                CREATE TABLE IF NOT EXISTS users (
                  uuid VARCHAR(255) NOT NULL,
                  first_name VARCHAR(255) NOT NULL,
                  last_name VARCHAR(255) NOT NULL,
                  phone VARCHAR(255) DEFAULT NULL,
                  email VARCHAR(255) DEFAULT NULL,
                  address TEXT NOT NULL,
                  registered_at DATETIME NOT NULL,
                  PRIMARY KEY (uuid)
                )
            ');
            $this->logger->log('Successfully connect to database sqlite:db.sqlite3.');
        }catch(\PDOException $e) {
            $this->logger->log('Database connection error: '.$e->getMessage());
        }
    }
    
    public function createUser(User $User){
        
        $this->pdo->exec("INSERT INTO users (uuid, first_name, last_name, email, phone, address, registered_at) VALUES 
          (
            '".$User->getUuid()."', 
            '".$User->getFirstName()."', 
            '".$User->getLastName()."', 
            '".$User->getEmail()."', 
            '".$User->getPhone()."', 
            '".json_encode($User->getLocation(),true)."', 
            '".$User->getRegisteredAt()->format('Y-m-d H:i:s')."'
        )");
    }
    
    public function removeUser(User $User) {
        $this->pdo->exec('DELETE FROM users WHERE uuid = "' . $User->getUuid() . '"');;
    }
    
    public function findUser($uuid){
        $query = $this->pdo->prepare("SELECT * FROM users WHERE uuid='".$uuid."' LIMIT 1");
        $query->execute();
        $result = $query->fetch();
        $User = new User();
        $User->setUuid($result['uuid']);
        $User->setFirstName($result['first_name']);
        $User->setLastName($result['last_name']);
        $User->setEmail($result['email']);
        $User->setPhone($result['phone']);
        $User->setLocation(json_decode($result['address'],true));
        $User->setRegisterdAt(\DateTime::createFromFormat('Y-m-d H:i:s', $result['registered_at']));
        return $User;
    }
    
    public function findAllUser(){
        $users = array();
        foreach ($this->pdo->query('SELECT * FROM users')->fetchAll() as $row) {
            $User = new User();
            $User->setUuid($row['uuid']);
            $User->setFirstName($row['first_name']);
            $User->setLastName($row['last_name']);
            $User->setEmail($row['email']);
            $User->setPhone($row['phone']);
            $User->setLocation(json_decode($row['address'], true));
            $User->setRegisterdAt(\DateTime::createFromFormat('Y-m-d H:i:s', $row['registered_at']));
            $users[]=$User;
        }        
        return $users;
    }
    
    public function cleanDb(){
        $this->pdo->exec('DELETE FROM users');
    }
}
