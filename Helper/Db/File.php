<?php

namespace Helper\Db;
use Helper\Db\DbInterface;
use Model\User;


class File implements DbInterface{
    
    private $users;
    
    public function __construct() {
        $this->users = include(DB_FILE);
    }
    
    public function saveFile() {
        file_put_contents(DB_FILE, sprintf("<?php\nreturn %s;", var_export($this->users, true)),LOCK_EX);
    }

    public function createUser(User $User){
        $this->users[$User->getUuid()] = $User;
        $this->saveFile();
    }
    
    public function removeUser(User $User) {
        unset($this->users[$User->getUuid()]);
        $this->saveFile();
    }
    
    public function findUser($uuid){
        return $this->users[$uuid];
    }
    
    public function findAllUser(){
        return $this->users;
    }
    
    public function cleanDb(){
        unlink(DB_FILE);
    }
}
