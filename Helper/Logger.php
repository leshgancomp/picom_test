<?php

namespace Helper;

class Logger {
    
    private $handle;
    
    public function __construct() {
        $this->handle = fopen('log', 'a');
    }
    
    public function __destruct() {
        fclose($this->handle);
    }

    public function log($message) {        
        fwrite($this->handle, $message . "\n");
    }

}
