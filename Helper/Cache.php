<?php
namespace Helper;

class Cache {
    
    public static function set($key, $value) {
        $path = CACHE_PATH . '/' . $key;
        if (!is_dir(dirname($path))) {
            mkdir(dirname($path), 0777, true);
        }
        file_put_contents($path, sprintf("<?php\nreturn %s;", var_export($value, true)));
    }

    public static function get($key) {
        $path = CACHE_PATH . '/' . $key;
        if (is_file($path)) {
            return include $path;
        }
        return null;
    }

    public static function has($key) {
        $path = CACHE_PATH . '/' . $key;
        return is_file($path);
    }

    public static function reset($key) {
        $path = CACHE_PATH . '/' . $key;
        if (is_file($path)) {
            unlink($path);
        }
    }

}
